package jp.alhinc.kudo_megumi.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class CalculateSales{
	public static void main(String[] args){

		if(args.length != 1){
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		HashMap<String, String> sales = new HashMap<String, String>();
		Map<String, Long> moneyMap = new HashMap<String, Long>();
		BufferedReader br = null;
		List<Integer> number = new ArrayList<Integer>();
		FileReader fr = null;
		String shop = "支店";

		if(!downloadingDeta(args[0], "branch.lst", sales, moneyMap, "支店", "\\d{3}")){
			return;
		}

		File dir = new File (args[0]);
		File[] list = dir.listFiles();
		List<String> lastList = new ArrayList<String>();

		for(int i = 0; i < list.length; i++){
			if(list[i].isFile() && list[i].getName().toString().matches("\\d{8}.rcd")){
				lastList.add(list[i].getName().toString()) ;
				String fileName = list[i].getName().toString();
				String fileNumber = fileName.substring(0,8);
				number.add(Integer.parseInt(fileNumber));
			}
		}

		for(int n = 0; n < number.size() -1; n++){
			if(number.get(n + 1) - number.get(n) != 1){
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}

		ArrayList<String> salesum = null;
		for(int j = 0; j < lastList.size(); j++){
			try{
				salesum = new ArrayList<String>();

				File salesFile = new File (args[0], lastList.get(j));
				fr = new FileReader (salesFile);
				br = new BufferedReader (fr);
				String saleString;
 				while((saleString = br.readLine()) != null){
					 salesum.add(saleString);
					}
				if(salesum.size() != 2){
					System.out.println(lastList.get(j) + "のフォーマットが不正です");
					return;
					}
				if(!salesum.get(1).matches("^\\d*$")){
					System.out.println("予期せぬエラーが発生しました");
					return;
					}
				if(!sales.containsKey(salesum.get(0))){
					System.out.println(lastList.get(j) + "の" + shop +"コードが不正です");
					return;
					}
				long aftersales = moneyMap.get(salesum.get(0)) + Long.parseLong(salesum.get(1));
				if(aftersales > 9999999999L){
					System.out.println("合計金額が10桁を超えました");
					return;
					}
				moneyMap.put(salesum.get(0), aftersales);
			} catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました。");
				return;
			} catch(NumberFormatException e) {
				System.out.println("予期せぬエラーが発生しました。");
					return;
			} finally {
				if(br != null) {
					try{
						br.close();
					} catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		}
		if(!fileOutput(args[0], "branch.out", sales, moneyMap)) {
			return;
		}
	}

	public static boolean downloadingDeta(String place, String inputName, HashMap<String, String> sales, Map<String,Long> moneyMap, String shop, String word){

		BufferedReader br = null;
		FileReader fr = null;

		try{ //支店定義ファイル読み込み
			File file = new File(place, inputName);

			if(!file.exists()){
				System.out.println(shop + "定義ファイルが存在しません");
				return false;
				}
			fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			while((line = br.readLine()) != null) {
				String[] items = line.split(",");
				if(!items[0].matches(word) || items.length != 2 ) {
					System.out.println(shop + "定義ファイルのフォーマットが不正です");
					return false;
				}
				sales.put(items[0], items[1]);
				moneyMap.put(items[0], 0L);
			}
		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if(br != null) {
				try {
					br.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}

	public static boolean fileOutput(String place, String outputName, HashMap<String, String> sales, Map<String,Long> moneyMap) {

		BufferedWriter bw = null;
		try {
			File newfile = new File (place, outputName);
			FileWriter fw = new FileWriter(newfile);
			bw = new BufferedWriter(fw);

			for(Map.Entry<String,String> entry : sales.entrySet()) {
				bw.write(entry.getKey() + "," + entry.getValue() + "," + moneyMap.get(entry.getKey()));
				bw.newLine();

			}
		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			try {
				if(bw != null) {
					bw.close();
				}
			} catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return false;
			}
		}
		return true;
	}
}